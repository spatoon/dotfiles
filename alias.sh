export SPICETIFY_INSTALL="$HOME/.spicetify"
export GOPATH="$HOME/go"
export PATH="$PATH:$HOME/.spicetify"
export PATH="$PATH:$HOME/go/bin"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$PATH:$HOME/.cargo/bin"
export PATH="$PATH:$HOME/.luarocks/bin"
export EDITOR="nvim"
export VISUAL="nvim"

addf() {
    if [ ! "$#" -gt 0 ]; then echo "Need a package to search for"; return 1; fi
    doas apk search -q "$1" | sk -m | xargs -r doas apk add
}
rgf() {
    if [ ! "$#" -gt 0 ]; then echo "Need a string to search for"; return 1; fi
    rg --files-with-matches --no-messages "$1" | sk --preview "bat --style=numbers --color=always {} | rg --colors 'match:bg:yellow' --color=always --ignore-case --context 10 '$1' || rg --ignore-case --pretty --context 10 '$1' {}"
}
send() {
    if [ ! "$#" -gt 0 ]; then echo "Need a file to upload"; return 1; fi
    scp -r "$1" ash@192.168.1.34:/home/ash
}
get() {
    if [ ! "$#" -gt 0 ]; then echo "Need a file to download"; return 1; fi
    scp -r ash@192.168.1.34:"$1" "$2"
}
play() {
    if [ ! "$#" -gt 0 ]; then echo "Need a video to play"; return 1; fi
    mpv "$1" & disown && exit
}
gpush() {
    if [ ! "$#" -gt 0 ]; then echo "Need a commit description"; return 1; fi
    git add . && git commit -m "$1" && git push
}
compile() {
    gcc "$1" -O2 -Wall -flto -fuse-ld=mold -march=native -mtune=native -pipe -masm=intel -Werror-implicit-function-declaration -fcf-protection -Wl,--color-diagnostics,--compress-debug-sections=zstd,--gc-sections,--pie -Werror=implicit-function-declaration -Werror=incompatible-pointer-types -Werror=return-type -fno-semantic-interposition -fpie -fstack-clash-protection -fstack-protector-strong -D_FORTIFY_SOURCE=2 -fmerge-constants -fvisibility=hidden -Wextra -Wpedantic
}
c() {
    if [ ! "$#" -gt 0 ]; then echo "Need a file to compile"; return 1; fi
    compile "$1" -s && strip -R .comment a.out
}
cg() {
    if [ ! "$#" -gt 0 ]; then echo "Need a file to compile"; return 1; fi
    compile "$1" -g -grecord-gcc-switches
}
tldr() {
  sh -c "tldr $1"
}
alias s="doas"
alias apk="s apk"
alias delf="cat /etc/apk/world | sk -m | xargs -r doas apk del"
alias gp="git pull --depth 1"
alias d="docker"
alias dc="docker-compose"
alias up="docker-compose up -d --remove-orphans"
alias serv="ssh -4 ash@192.168.1.34"
alias dserv="ssh -4 ash@83.198.132.68"
alias logs="docker-compose logs"
alias torrent='java -jar ./jack-of-all-trades-2.1.36.jar --joal-conf="." --spring.main.web-environment=true --server.port=3000 --joal.ui.path.prefix="3j5sd3m8nxBwav7kRtY4DsL2ZKj3ZA8PamCDx9BGRHsQmZw9SruvStQCWvH6ar4EoVS349FsKB39gDNZLDNn6GsZGvjoDRiKUbzuYQZL54N8wpqwWdw36guanw2Aw5rQ" --joal.ui.secret-token="Mkh4qnB6A7MjsfSJbbrKxegJFrdnMjNrQP6FTi3BysBvx23BW7dFKvU68RXMKGc7STeujHPtJBqWUNNQ5YCnWAoXoap3sRUzfQmvDBZFBHHyCdy8fAEtocLwW42kMD74" 2>/dev/null & disown'
alias n="nvim"
alias copy="rsync -hr --progress"
alias audio="yt-dlp --downloader aria2c --extract-audio --audio-quality 0"
alias video="yt-dlp --downloader aria2c"
alias r="cargo"
alias nc="nvim ~/.config/nvim/init.lua"
alias ali="nvim ~/alias.sh"
# alias nc="nvim --clean --headless --server /tmp/nvim-term-${NVIM_ADDRESS}.pipe --remote-send '<cmd> tabnew ~/.config/nvim/init.lua <CR>'"
# alias ali="nvim --clean --headless --server '/tmp/nvim-term-${NVIM_ADDRESS}.pipe' --remote-send '<cmd> tabnew ~/alias.sh <CR>'"
alias lt="la --tree"
alias a="nasm"
alias m="mold"
alias p="python3"
alias ex="7z x"
alias pip="uv pip"
alias f="flatpak --user"
alias open="xdg-open"
alias cd="z"
alias gcl="git clone --depth=1"
alias w="aria2c"
alias no="s rm -rf"
alias :q="exit"
alias q="exit"
alias reload="source ~/.zshrc"
alias ls="eza --icons --color=always"
alias lf="ls -d"
alias ll="ls -f"
alias la="ls --long --all"
alias lag="la --git"
alias las="la --total-size"
alias nano="n"
alias vi="n"
alias vim="n"
alias poweroff="s poweroff"
alias shutdown="s shutdown now"
alias reboot="s reboot"
alias sn="s nvim"
alias cls="clear"
alias htop="s htop"
alias bat="bat --paging='never' --style=numbers --color=always"
alias rg="rg --hidden --no-ignore -i --no-messages"
alias g="git"
alias nf="rg --hidden --files | sk --preview 'bat --style=numbers --color=always --line-range :100 {}' | xargs -r nvim"
alias cdf='directory=$(find . -type d 2>&1 | grep -v "Permission" | sed "s/^.\///" | sk --preview="eza --icons --color=always {}") && cd "$directory" || echo "Failed to change directory"'
alias hist='print -z "$(sort ~/.zsh/.histfile | uniq | sk)"'
alias j="just"
alias gp="git pull --depth 1"
alias nclean="nvim --cmd 'lua vim.g.lazy_did_setup = true'"

export FZF_DEFAULT_OPTS='
--color=fg:#A0A8CD,hl:#bb9af7
--color=fg+:#A0A8CD,bg+:#212234,hl+:#7dcfff
--color=info:#7aa2f7,prompt:#7dcfff,pointer:#7dcfff
--color=marker:#9ece6a,spinner:#9ece6a,header:#9ece6a
--ansi --border=rounded --info=right'

export SKIM_DEFAULT_OPTIONS="$SKIM_DEFAULT_OPTIONS \
  --color=fg:#A0A8CD,matched:#7dcfff,matched_bg:#7aa2f7,current:#D0D8F0,current_bg:#212234,current_match:#7dcfff,current_match_bg:#7aa2f7,spinner:#9ece6a,info:#7aa2f7,prompt:#7dcfff,cursor:#7dcfff,selected:#9ece6a,header:#9ece6a,border:#6c7086"
