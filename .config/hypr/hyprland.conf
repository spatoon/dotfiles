# See https://wiki.hyprland.org/Configuring/Monitors/

env = WLR_NO_HARDWARE_CURSORS, 1

monitor=HDMI-A-2,1920x1080@165,0x0,1
# monitor=eDP-1,1920x1080@60,0x0,1.25
# monitor=,prefered,auto,1.25,mirror,eDP-1

# Execute your favorite apps at launch
exec-once = dbus-update-activation-environment DISPLAY WAYLAND_DISPLAY
exec-once = waybar
exec-once = foot --server
exec-once = wl-paste --watch cliphist store 
exec-once = pipewire
exec-once = pipewire-pulse
exec-once = wireplumber 
exec-once = wbg ~/.config/wallpaper/pixel-art.jpeg
exec-once = mako -c ~/.config/mako/config
exec-once = kanata -c ~/.config/kanata/kanata.kbd -n

# Some default env vars.
env = HYPRCURSOR_THEME,HyprBibataModernClassicSVG
env = HYPRCURSOR_SIZE,20
env = XCURSOR_SIZE,20
env = QT_QPA_PLATFORMTHEME,qt6ct
env = QT_QPA_PLATFORM,wayland

# Rose pine colors
# $base           = 0xff191724
# $surface        = 0xff1f1d2e
# $overlay        = 0xff26233a
$muted          = 0xff6e6a86
# $subtle         = 0xff908caa
# $text           = 0xffe0def4
# $love           = 0xffeb6f92
# $gold           = 0xfff6c177
# $rose           = 0xffebbcba
# $pine           = 0xff31748f
# $foam           = 0xff9ccfd8
$iris           = 0xffc4a7e7
# $highlightLow   = 0xff21202e
# $highlightMed   = 0xff403d52
# $highlightHigh  = 0xff524f67

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {

  kb_layout = fr,us
  kb_options = caps:escape
		
	numlock_by_default = true

	repeat_delay = 350

  follow_mouse = 2

  touchpad {
    natural_scroll = yes
    disable_while_typing = false
  }
	
	accel_profile = flat
  sensitivity = 0.2 # -1.0 - 1.0, 0 means no modification.
}

general {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more

  gaps_in = 5
  gaps_out = 5
  border_size = 2

  col.active_border = $iris
  col.inactive_border = $muted

  # col.active_border = rgba(D1CEDAFF) 45deg
  # col.inactive_border = rgba(5B5666FF)

  layout = dwindle

  # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
  allow_tearing = false
}

decoration {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more

  rounding = 4
  blur {
    size = 7
    passes = 4
    popups = true
    brightness = 0.65
    ignore_opacity = on
    contrast = 1.5
  }

  shadow {
    enabled = false
    range = 4
    render_power = 3
    color = rgba(000000ff)
  }
}

xwayland {
  force_zero_scaling = true
}

animations {
  enabled = yes

	bezier = overshot, 0.05, 0.9, 0.1, 1.05
	bezier = smoothOut, 0.36, 0, 0.66, -0.56
	bezier = smoothIn, 0.25, 1, 0.5, 1

	animation = windows, 1, 4, overshot, slide
	animation = windowsOut, 1, 3, smoothOut, slide
	animation = windowsMove, 1, 3, smoothIn
	animation = border, 1, 6, smoothIn
	animation = fade, 1, 2.5, smoothIn
  animation = fadeOut, 0, 0, 
	animation = fadeDim, 0, 3, smoothIn
	animation = workspaces, 1, 4, smoothIn
}

dwindle {
  # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
  pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
  preserve_split = yes # you probably want this
}

gestures {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more
  workspace_swipe = yes
}

group { 
  groupbar {
    enabled = false
  }
}

misc {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more
  force_default_wallpaper = 0 # Set to 0 to disable the anime mascot wallpapers
	disable_hyprland_logo = true # Sowwy Hypr-chan ;3
  mouse_move_enables_dpms = true
  key_press_enables_dpms = true
  disable_autoreload = true
  new_window_takes_over_fullscreen = 2
	vrr = 1
}

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = , F11, fullscreen
bind = $mainMod, A, exec, footclient
bind = $mainMod, W, exec, librewolf
bind = $mainMod, B, exec, blueman-manager
bind = $mainMod, V, exec, flatpak --user run com.valvesoftware.Steam
bind = $mainMod SHIFT, L, exec, hyprlock
bind = $mainMod, R, exec, footclient pulsemixer
bind = $mainMod, F3, exec, grim -l 9 -g "$(slurp)"
bind = $mainMod, F2, exec, grim -l 9
bind = $mainMod SHIFT, F1, exec, doas reboot
bind = $mainMod, D, exec, flatpak --user run xyz.armcord.ArmCord 
bind = $mainMod, G, exec, ~/.config/hypr/gamemode.sh
bind = $mainMod, F, exec, flatpak --user run com.spotify.Client
bind = $mainMod, S, exec, tofi-drun -c /home/user/.config/tofi/tofi.conf
bind = $mainMod, X, exec, flatpak --user run com.brave.Browser
bind = $mainMod, N, exec, neovide
bind = $mainMod, Z, exec, flatpak --user run com.heroicgameslauncher.hgl --ozone-platform=wayland
bind = $mainMod SHIFT, Z, exec, gamemoderun flatpak --user run org.vinegarhq.Sober
bind = $mainMod, C, killactive,
bind = $mainMod SHIFT, N, exit,
bind = $mainMod, Y, togglefloating,
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, H, togglesplit, # dwindle

bindl=, XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-
bindl=, XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+

bindl=, XF86MonBrightnessUp, exec, brightnessctl set +5%
bindl=, XF86MonBrightnessDown, exec, brightnessctl set 5%-

bind = $mainMod, j, movefocus, l
bind = $mainMod, m, movefocus, r
bind = $mainMod, l, movefocus, u
bind = $mainMod, k, movefocus, d

# Switch workspaces with mainMod + [0-9]
bind = $mainMod, ampersand, workspace, 1
bind = $mainMod, eacute, workspace, 2
bind = $mainMod, quotedbl, workspace, 3
bind = $mainMod, apostrophe, workspace, 4
bind = $mainMod, parenleft, workspace, 5
bind = $mainMod, minus, workspace, 6
bind = $mainMod, egrave, workspace, 7
bind = $mainMod, underscore, workspace, 8
bind = $mainMod, ccedilla, workspace, 9
bind = $mainMod, agrave, workspace, 10

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, ampersand, movetoworkspace, 1
bind = $mainMod SHIFT, eacute, movetoworkspace, 2
bind = $mainMod SHIFT, quotedbl, movetoworkspace, 3
bind = $mainMod SHIFT, apostrophe, movetoworkspace, 4
bind = $mainMod SHIFT, parenleft, movetoworkspace, 5
bind = $mainMod SHIFT, minus, movetoworkspace, 6
bind = $mainMod SHIFT, egrave, movetoworkspace, 7
bind = $mainMod SHIFT, underscore, movetoworkspace, 8
bind = $mainMod SHIFT, ccedilla, movetoworkspace, 9
bind = $mainMod SHIFT, agrave, movetoworkspace, 10

# Example special workspace (scratchpad)
# bind = $mainMod, S, togglespecialworkspace, magic
# bind = $mainMod SHIFT, S, movetoworkspace, special:magic

# Scroll through existing workspaces with mainMod + scroll
# bind = $mainMod, mouse_down, workspace, e+1
# bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

layerrule = blur,launcher
layerrule = blur,logout_dialog
layerrule = animation slide,waybar

windowrulev2 = noanim,class:(ueberzug)

windowrulev2 = opacity 0.0 override 0.0 override,class:^(xwaylandvideobridge)$
windowrulev2 = noanim,class:^(xwaylandvideobridge)$
windowrulev2 = noinitialfocus,class:^(xwaylandvideobridge)$
windowrulev2 = maxsize 1 1,class:^(xwaylandvideobridge)$
windowrulev2 = noblur,class:^(xwaylandvideobridge)$
