doas npm i -g tldr
doas npm i -g neovim
sh -c "bat cache --build"
flatpak install xyz.armcord.Armcord
flatpak install org.onlyoffice.desktopeditors
flatpak install org.libreoffice.libreoffice
flatpak install org.gtk.Gtk3theme.adw-gtk3-dark
flatpak install com.spotify.Client
flatpak install com.github.tchx84.Flatseal
flatpak install com.brave.Browser
curl -fsSL https://raw.githubusercontent.com/spicetify/cli/main/install.sh | sh
curl -fsSL https://raw.githubusercontent.com/spicetify/marketplace/main/resources/install.sh | sh
spicetify apply
