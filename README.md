
![rice.jpg](preview/rice.jpg)

## Stuff in this config

- **WM** -> [Hyprland](https://hyprland.org)
- **Theme** -> [Rosé pine](https://rosepinetheme.com/)
- **Launcher** -> [Tofi](https://github.com/philj56/tofi)
- **IDE** -> [Neovim](https://gitlab.com/spatoon/nvim-config)
- **Bar** -> [Waybar](https://github.com/Alexays/Waybar)
- **Distro** -> [Alpine](https://alpinelinux.org/)

## Weird stuff you'll need

- /etc/apk/world
- /etc/install.sh
