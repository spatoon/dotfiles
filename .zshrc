# History Configuration
HISTFILE=~/.zsh/.histfile
HISTSIZE=10000
SAVEHIST=10000

setopt INTERACTIVE_COMMENTS # comments in interactive mode, useful for copypasting
setopt hist_ignore_all_dups # Ignore duplicate commands
setopt hist_reduce_blanks   # Remove extra blanks in history
setopt share_history        # Share history across terminals
setopt autocd               # Allow directory auto-navigation
unsetopt beep               # Disable terminal bell

# Colorized: Command not found
function command_not_found_handler() {
	print "\e[1;33mCommand not found: \e[1;31m$1\e[0m"
	return 127
}

# Source Aliases
source ~/alias.sh

# Auto-completion Configuration
zstyle :compinstall filename '/home/user/.zshrc'
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' menu no # let fzf-tab handle completions
zstyle ':completion:*:git-checkout:*' sort false # disable sort when completing `git checkout`
zstyle ':completion:*:descriptions' format '[%d]' # ignore escape sequences (like '%F{red}%d%f')
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS} # set list-colors to enable filename colorizing
zstyle ':fzf-tab:*' use-fzf-default-opts yes # make fzf-tab follow FZF_DEFAULT_OPTS.
zstyle ':fzf-tab:*' switch-group '<' '>' # switch group using `<` and `>`
_comp_options+=(globdots)

# Additional Completion Paths
fpath=(~/.zsh/zsh-completions/src $fpath)

# Use Neovim for Manual Pages
export MANPAGER='nvim +Man!'

# Plugin Management with Znap
[[ -r ~/Repos/znap/znap.zsh ]] || 
  git clone --depth 1 https://github.com/marlonrichert/zsh-snap.git ~/Repos/znap
source ~/Repos/znap/znap.zsh

# Load Plugins
znap install aureliojargas/clitest # Cli tester
znap install zsh-users/zsh-completions # More completions
znap source Aloxaf/fzf-tab # fzf completions
znap source MichaelAquilina/zsh-auto-notify # notify if command time > 60s
znap source MichaelAquilina/zsh-you-should-use # Warns you if use a command that you have an alias for
znap source jeffreytse/zsh-vi-mode # Better vim mode
znap source zsh-users/zsh-autosuggestions # fish / copilot suggestions
znap source zsh-users/zsh-history-substring-search # search through history easely
znap source zsh-users/zsh-syntax-highlighting # syntax-highlighting

znap source hlissner/zsh-autopair # autopairs "" '' () [] {} ...
source /home/user/Repos/hlissner/zsh-autopair/autopair.plugin.zsh

znap source marlonrichert/zcolors # Use $LS_COLORS for coherent themes inside less git grep
znap eval zcolors "zcolors ${(q)LS_COLORS}"


# Key Bindings
bindkey -v # Use Vim mode key bindings
bindkey '^[[A' history-substring-search-up # Up and down arrows
bindkey '^[[B' history-substring-search-down

# Prompt and Git Info
autoload -U add-zsh-hook
add-zsh-hook precmd vcs_info # Update vcs_info before prompt

setopt prompt_subst # Allow variable substitution in prompt
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:git*' formats '%b%u%c'
zstyle ':vcs_info:git*' actionformats '%F{14}⏱ %*%f'
zstyle ':vcs_info:git*' unstagedstr '*'
zstyle ':vcs_info:git*' stagedstr '+'
zstyle ':vcs_info:*:*' check-for-changes true

# Prompt Configuration
RPROMPT='%F{8}⎇  $vcs_info_msg_0_' # Right prompt shows Git status
PROMPT='%F{blue}%4~ %F{magenta}$ ' # Left prompt shows current dir

# Aliases
alias profile="nvim ~/.zshrc"

# Auto-notify Configuration
export AUTO_NOTIFY_IGNORE=("rgf" "nf" "create-nvim-term" "xdg-open" "docker exec" "docker run" "docker-compose run" "docker-compose exec" "docker logs" "docker-compose logs" "man" "sleep" "nvim" "git" "ssh")
export AUTO_NOTIFY_THRESHOLD=60  # Only notify if command takes over 60 seconds

# Zoxide Initialization
znap eval zoxide "zoxide init zsh"

# Zellij auto start
# znap eval zellij "zellij setup --generate-auto-start zsh"

# Starship config
# export STARSHIP_CONFIG=~/.config/starship/starship.toml
# export STARSHIP_LOG="error" # Reduce Starship log level
# znap eval starship "starship init zsh"
