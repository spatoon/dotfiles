if [ -z "$XDG_RUNTIME_DIR" ]; then
  XDG_RUNTIME_DIR="/tmp/$(id -u)-runtime-dir"
  mkdir -pm 0700 "$XDG_RUNTIME_DIR"
  export XDG_RUNTIME_DIR
fi

# XDG_CURRENT_DESKTOP=dwl dbus-run-session .config/dwl/dwl
[ "$(tty)" = "/dev/tty1" ] && dbus-run-session Hyprland
